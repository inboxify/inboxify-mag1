# Inboxify Magento 1 API Client

This repository contains official Inboxify client for Magento 1.

## Requirements

- PHP >= 5.3
- [PHP module HASH](http://php.net/manual/en/book.hash.php)
- [PHP module JSON](http://php.net/manual/en/book.json.php)
- [PHP module Sockets](http://php.net/manual/en/book.sockets.php)
- [Inboxify API Account](https://www.inboxify.nl/)
- Magento version 1.6 or higher

## Installation

You can install module using one of the following methods.

### Magento Connect

- [Professio Inboxify Integration](https://www.magentocommerce.com/magento-connect/professio-inboxify-integration-1.html)

### Modman

In root of your Magento installation initiate modman if you didn't do it already:

- `modman init`

Then clone the extension from repository:

- `modman clone git@gitlab.com:inboxify/inboxify-mag1.git`

## Configuration

After module installation go to Magento Configuration back-end, then in menu
select `System` -> `Configuration`, and click on `Inboxify` in `Customers` tab.

The required configuration directives are: `API Endpoint`, `API Key`, `API Secret` and `List`.

After initial API configuration is saved, the list of mailing lists will 
be loaded automatically.

## Copyright

MIT License

## Contact Information

- Email: [info@inboxify.nl](mailto:info@inboxify.nl)
- Website: [Inboxify](https://www.inboxify.nl/)

## Changelog

- 1.0.1 (2016-06-30):
    - Changed API end-point, emails, URLs from inboxify.eu to inboxify.nl

- 1.0.0 (2016-12-20):
    - Initial version
