<?php
/**
 * Professio_Inboxify extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag1/blob/master/LICENSE
 * 
 * @category       Professio
 * @package        Professio_Inboxify
 * @copyright      Copyright (c) 2016 - 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag1/blob/master/LICENSE
 */

/**
 * Data helper
 *
 * @category    Professio
 * @package     Professio_Inboxify
 */
class Professio_Inboxify_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Test http headers signature
     * 
     * @param array $headers
     * @return boolean
     */
    public function checkSignature($headers)
    {
        $signature = hash_hmac(
            'sha256', 
            $headers['SALT'], 
            Mage::helper('inboxify/config')->getApiSecret(),
            true
        );
        
        $signatureEncoded = base64_encode($signature);
        
        Mage::log(
            'inboxify/data_helper::checkSignature() '
            . 'headers: ' . json_encode($headers)
            . 'signature: ' . $signature
            . 'signature encoded: ' 
            . str_replace('%3d', '=', $signatureEncoded)
        );
        
        return $signatureEncoded 
            == str_replace('%3d', '=', $signatureEncoded);
    }
    
    /**
     * Get order tags (ordered products category names)
     * 
     * @param Mage_Sales_Model_Order $order
     * @return array
     */
    public function getOrderTags(Mage_Sales_Model_Order $order)
    {
        $items = $order->getAllItems();
        $orderTags = array();

        foreach ($items as $item) {
            $product = $item->getProduct();
            $categoryCollection = $product->getCategoryCollection();
            $categoryCollection->addAttributeToSelect('name');
            $categoryCollection->clear()->load();

            foreach ($categoryCollection->getIterator() as $category) {
                $orderTags[] = $category->getName();
            }
        }
        
        return $orderTags;
    }
    
    /**
     * Retrieve customer's primary address of configured type
     * @param Mage_Customer_Model_Customer $customer
     * @return Mage_Customer_Model_Address
     */
    public function getCustomersPrimaryAddress(
        Mage_Customer_Model_Customer $customer
    )
    {
        $addressType = Mage::helper('inboxify/config')
            ->getAdvancedAddressType();
        
        Mage::log(
            'inboxify/data_helper::getCustomersPrimaryAddress() '
            . 'type: ' . $addressType
        );
        
        switch($addressType) {
            case 'billing':
                $address = $customer->getPrimaryBillingAddress();
                break;
            case 'shipping':
                $address = $customer->getPrimaryShippingAddress();
                break;
        }
        
        Mage::log(
            'inboxify/data_helper::getCustomersPrimaryAddress() '
            . ' address id: ' . ( $address ? $address->getEntityId() : 'no' )
        );
        
        return $address;
    }
    
    /**
     * Get list of category names of all ordered products by customer
     * @param Mage_Customer_Model_Customer $customer
     * @return type
     */
    public function getCategoryNamesOfOrderedProducts(
        Mage_Customer_Model_Customer $customer
    )
    {
        $states = Mage::getSingleton('sales/order_config')
            ->getVisibleOnFrontStates();
        
        $collection = Mage::getResourceModel('sales/order_collection')
            ->addFieldToSelect('*')
            ->addFieldToFilter('customer_id', $customer->getId())
            ->addFieldToFilter('state', array('in' => $states))
            ->setOrder('created_at', 'desc');
        
        $collection->load();
        
        $tags = array();
        
        foreach ($collection->getIterator() as $order) {
            $tags = array_merge($tags, $this->getOrderTags($order));
        }
        
        return array_unique($tags);
    }
}
