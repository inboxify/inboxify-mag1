<?php
/**
 * Professio_Inboxify extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag1/blob/master/LICENSE
 * 
 * @category       Professio
 * @package        Professio_Inboxify
 * @copyright      Copyright (c) 2016 - 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag1/blob/master/LICENSE
 */

/**
 * Observer model
 * 
 * @category   Professio
 * @package    Professio_Inboxify
 */

class Professio_Inboxify_Model_Observer
{
    /**
     * Get Inboxify API client
     * 
     * @return \Inboxify\Api\Client
     */
    protected function getClient()
    {
        return Mage::getSingleton('inboxify/client')->getClient();
    }
    
    /**
     * Get request
     * 
     * @return Mage_Core_Controller_Request_Http
     */
    protected function getRequest()
    {
        return Mage::app()->getRequest();
    }
    
    /**
     * Get current session
     * 
     * @return Mage_Core_Model_Session_Abstract
     */
    protected function getSession()
    {
        if (!isset($this->session)) {
            if (Mage::app()->getStore()->isAdmin()) {
                $this->session = Mage::getSingleton('adminhtml/session');
            } else if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                $this->session = Mage::getSingleton('customer/session');
            } else {
                $this->session = Mage::getSingleton('core/session');
            }
        }
        
        return $this->session;
    }
    
    /**
     * Log wrapper - loging only when in developer mode
     * 
     * @param type $message
     */
    protected function log($message)
    {
        if (Mage::getIsDeveloperMode()) {
            Mage::log($message);
        }
    }
    
    /**
     * Check if current address is the right type.
     * @param type $address
     * @param type $customer
     */
    protected function isAddressSaveAfter($address, $customer)
    {
        if ($customer->getDefaultBillingAddress()) {
            $billing = $address->getEntityId() == $customer
                ->getDefaultBillingAddress()->getEntityId()
                && Mage::helper('inboxify/config')
                    ->isAddressTypeBilling();
        } else {
            $billing = false;
        }

        if ($customer->getDefaultShippingAddress()) {
            $shipping = $address->getEntityId() == $customer
                ->getDefaultShippingAddress()->getEntityId()
                && Mage::helper('inboxify/config')
                    ->isAddressTypeShipping();
        } else {
            $shipping = false;
        }
        
        return $billing || $shipping;
    }
    
    /**
     * After address save, check if primary and update contact
     * 
     * @param Varien_Event_Observer $observer
     */
    public function addressSaveAfter($observer)
    {
        $this->log('inboxify/observer::addressSaveAfter() start');
        
        $request = $this->getRequest();
        
        if ('customer' == $request->getModuleName() 
            && 'account' == $request->getControllerName() 
            && 'editPost' == $request->getActionName()
        ) {
            // INFO if saving customer don't fire this event
            $this->log(
                'inboxify/observer::addressSaveAfter() skip duplic. event'
            );
            
            return;
        }
        
        try {
            if (Mage::helper('inboxify/config')
                    ->isAdvancedOnAddressUpdateEnabled()
                ) {
                $address = $observer->getCustomerAddress();
                $customer = $address->getCustomer();
                
                if ($this->isAddressSaveAfter($address, $customer)) {

                    if ($customer && $customer->getEntityId()) {
                        $client = $this->getClient();
                        $contact = $client->getContact($customer->getEmail());

                        if ($contact) {
                            Mage::helper('inboxify/mapper')
                                ->addressToContact(
                                    $address, $contact
                                );

                            $client->putContact($contact->email, $contact);
                        }
                    } else {
                        $this->log(
                            'inboxify/observer::addressSaveAfter() no cust.'
                        );
                    }
                } else {
                    $this->log(
                        'inboxify/observer::addressSaveAfter() wrong type'
                    );
                }
            } else {
                $this->log(
                    'inboxify/observer::addressSaveAfter() disabled'
                );
            }
        } catch (Exception $e) {
            $this->getSession()->addError($e->getMessage());
            $this->log(
                'inboxify/observer::addressSaveAfter() '
                . 'failed with exception: ' . $e->getMessage()
            );
            Mage::logException($e);
        }
        
        $this->log('inboxify/observer::addressSaveAfter() end');
    }
    
    /**
     * After customer delete - if enabled by config delete contact after 
     * deleting customer
     * 
     * @param Varien_Event_Observer $observer
     */
    public function customerDeleteAfter($observer)
    {
        $this->log('inboxify/observer::customerDeleteAfter() start');
        
        try {
            if (Mage::helper('inboxify/config')
                    ->isAdvancedOnCustomerDeleteEnabled()
                || Mage::helper('inboxify/config')
                    ->isAdvancedOnCustomerDeleteUnsubscribeEnabled()
                ) {
                $customer = $observer->getCustomer();
                $client = Mage::getSingleton('inboxify/client')
                    ->getStoreClient($customer->getStoreId());
                $email = $customer->getEmail();
                $contact = $client->getContact($email);
                
                if ($contact) {
                    if (Mage::helper('inboxify/config')
                        ->isAdvancedOnCustomerDeleteUnsubscribeEnabled()) {
                        $contact->subscribe = false;
                        $contact->unsubscribed = true;
                        
                        $client->putContact($contact->email, $contact);
                        
                        $this->log(
                            'inboxify/observer::customerDeleteAfter() '
                            . 'unsubscribe / delete contact for customer id: '
                            . $customer->getEntityId()
                        );
                    }
                    
                    $client->deleteContact($email);
                    
                    $this->log(
                        'inboxify/observer::customerDeleteAfter() '
                        . 'deleted contact for customer id: '
                        . $customer->getEntityId()
                    );
                } else {
                    $this->log(
                        'inboxify/observer::customerDeleteAfter() '
                        . 'contact not found for customer id: '
                        . $customer->getEntityId()
                    );
                }
            } else {
                $this->log(
                    'inboxify/observer::customerDeleteAfter() disabled'
                );
            }
        } catch (Exception $e) {
            $this->getSession()->addError($e->getMessage());
            $this->log(
                'inboxify/observer::customerDeleteAfter() '
                . 'failed with exception: ' . $e->getMessage()
            );
            Mage::logException($e);
        }
        
        $this->log('inboxify/observer::customerDeleteAfter() end');
    }
    
    /**
     * Admin - Customer save after - if there are post data for contact 
     * update contact. If update customer contact config is enabled,
     * update contact by customer data
     * 
     * @param Varien_Event_Observer $observer
     */
    public function customerSaveAfterAdmin($observer)
    {
        $this->log('inboxify/observer::customerSaveAfterAdmin() start');
        
        try {
            if (!Mage::helper('inboxify/config')
                ->isAdvancedOnCustomerUpdateEnabled() ) {
                // nothing to do
                $this->log(
                    'inboxify/observer::customerSaveAfterAdmin() disabled'
                );
                return;
            }
            
            $customer = $observer->getEvent()->getCustomer();
            $client = Mage::getSingleton('inboxify/client')
                ->getStoreClient($customer->getStoreId());
            
            $email = $customer->getOrigData('email') 
                ? $customer->getOrigData('email')
                : $customer->getData('email');

            $contact = $client->getContact($email);
            $subscribe = $this->getRequest()->getPost('inboxify_subscribe');
            
            if (!$contact && !$subscribe) {
                // nothing to do 2
                return;
            }
            
            if (!$contact) {
                $contact = new stdClass();
                $new = true;
            } else {
                // unsub only if contact
                $new = false;
            }

            Mage::helper('inboxify/mapper')
                ->customerToContact($customer, $contact);
            
            $contact->subscribe = $subscribe;
            $contact->unsubscribed = !$subscribe;
            
            if ($new) {
                $client->postContact($contact);
            } else {
                $client->putContact($email, $contact);
            }
            
        } catch (Exception $e) {
            $this->getSession()->addError($e->getMessage());
            $this->log(
                'inboxify/observer::customerSaveAfterAdmin() '
                . 'failed with exception: ' . $e->getMessage()
            );
            Mage::logException($e);
        }
        
        $this->log('inboxify/observer::customerSaveAfterAdmin() end');
    }
    
    /**
     * Front - customer save after. If update of contact by customer is enabled 
     * update contact. If there is subscribed parameter after registering new 
     * customer, sign up the customer.
     * 
     * @param Varien_Event_Observer $observer
     */
    public function customerSaveAfterFront($observer)
    {
        $this->log('inboxify/observer::customerSaveAfterFront() start');
        
        try {
            if (!Mage::helper('inboxify/config')
                ->isAdvancedOnCustomerUpdateEnabled() ) {
                // nothing to do
                $this->log(
                    'inboxify/observer::customerSaveAfterFront() disabled'
                );
                return;
            }
            
            $client = $this->getClient();
            $customer = $observer->getEvent()->getCustomer();
            
            $email = $customer->getOrigData('email') 
                ? $customer->getOrigData('email')
                : $customer->getData('email');

            $contact = $client->getContact($email);
            
            $subscribe = Mage::app()->getRequest()->getPost('iy_is_subscribed');
            
            if ($contact) {
                Mage::helper('inboxify/mapper')->customerToContact(
                    $customer, $contact
                );
                
                $client->putContact($email, $contact);
            } else if ($subscribe) {
                $contact = new stdClass();
                
                Mage::helper('inboxify/mapper')->customerToContact(
                    $customer, $contact
                );
                
                $contact->subscribe = $subscribe;
                $contact->unsubscribed = !$subscribe;
                
                $client->postContact($contact);
            }
        } catch (Exception $e) {
            $this->getSession()->addError($e->getMessage());
            $this->log(
                'inboxify/observer::customerSaveAfterFront() '
                . 'failed with exception: ' . $e->getMessage()
            );
            Mage::logException($e);
        }
        
        $this->log('inboxify/observer::customerSaveAfterFront() end');
    }
    
    /**
     * After place order - if enabled, update contact by tags (ordered product 
     * category names)
     * 
     * @param Varien_Event_Observer $observer
     */
    public function salesOrderPlaceAfter($observer)
    {
        $this->log('inboxify/observer::salesOrderPlaceAfter() start');
        
        try {
            $subscribe = Mage::app()->getRequest()
                ->getPost('iy_is_subscribed');
	    if (!$subscribe)
                $subscribe = Mage::app()->getRequest()
	            ->getPost('subscribe_newsletter');
            
            $client = Mage::getSingleton('inboxify/client')->getClient();
            
            $order = $observer->getEvent()->getOrder();
            $email = $order->getCustomerEmail();
            $contact = $client->getContact($email);
            
            if ($subscribe) {
                if (!$contact) {
                    $contact = new stdClass();

                    Mage::helper('inboxify/mapper')
                        ->orderToContact($order, $contact);
                    
                    $new = true;
                } else {
                    $new = false;
                }
                
                $contact->subscribe = $subscribe;
                $contact->unsubscribed = !$subscribe;

                if ($new) {
                    $client->postContact($contact);
                } else {
                    $client->putContact($contact->email, $contact);
                }
            }

            if (Mage::helper('inboxify/config')
                ->isAdvancedOnOrderEnabled()) {

                if ($contact) {
                    $orderTags = Mage::helper('inboxify')
                        ->getOrderTags($order);
                    
                    $client->postTags($email, $orderTags);
                }
            } else {
                $this->log(
                    'inboxify/observer::salesOrderPlaceAfter() '
                    . 'tagging disabled'
                );
            }
        } catch(Exception $e) {
            $this->getSession()->addError($e->getMessage());
            $this->log(
                'inboxify/observer::salesOrderPlaceAfter() '
                . 'failed with exception: ' . $e->getMessage()
            );
            Mage::logException($e);
        }
        
        $this->log('inboxify/observer::salesOrderPlaceAfter() end');
    }
    
    /**
     * After saving configuration of Inboxify - validate API key 
     * and secret. Optionally flush cache (if disabled).
     * 
     * @param Varien_Event_Observer $observer
     */
    public function afterConfigChange()
    {
        $this->log('inboxify/observer::afterConfigChange() start');
        
        try {
            if (!$this->getClient()->isConnected()) {
                $this->getSession()->addError(
                    Mage::helper('inboxify')
                    ->__('Invalid API endpoint, key and secret combination.')
                );
            } else {
                $this->getSession()->addSuccess(
                    Mage::helper('inboxify')
                        ->__(
                            'API endpoint, key and secret combination is valid.'
                        )
                );
            }
            
            if (!Mage::helper('inboxify/config')->isCacheEnabled()) {
                $this->getClient()->getCache()->purge();
            }
        } catch (Exception $e) {
                $this->getSession()->addError($e->getMessage());
                $this->log(
                    'inboxify/observer::afterConfigChange() failed '
                    . 'with exception: ' . $e->getMessage()
                );
                Mage::logException($e);
        }
        
        $this->log('inboxify/observer::afterConfigChange() end');
    }
}
