<?php
/**
 * Professio_Inboxify extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag1/blob/master/LICENSE
 * 
 * @category       Professio
 * @package        Professio_Inboxify
 * @copyright      Copyright (c) 2015 - 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag1/blob/master/LICENSE
 */

if (Mage::getIsDeveloperMode()) {
    Mage::log('inboxify_setup started');
}

/**
 * @var Professio_Inboxify_Model_Resource_Setup $this
 */
$this->startSetup();

$connection = $this->getConnection();
$resource = Mage::getSingleton('core/resource');

try {
    $connection->beginTransaction();
    
    $tableName = $this->getTable('core/config_data');

    $query = 'SELECT * FROM ' . $tableName . ' WHERE path LIKE "budgetmailer%"';
    $results = $connection->fetchAll($query);

    if ($results && count($results)) {
        foreach($results as $result) {
            if ('budgetmailer/api/endpoint' == $result['path']) {
                $result['value'] = 'https://api.inboxify.nl/';
            }
            
            unset($result['config_id']);
            $result['path'] = str_replace('budgetmailer', 'inboxify', $result['path']);
            
            $insert = 'INSERT INTO ' . $tableName . ' VALUES (NULL, "' 
                . $result['scope'] . '", "' . $result['scope_id'] . '", "'
                . $result['path'] . '", "' . $result['value'] . '")';
            
            $connection->query($insert);
        }
    }
    
    $connection->commit();
} catch (Exception $e) {
    $connection->rollback();
    Mage::logException($e);
}

$this->endSetup();

if (Mage::getIsDeveloperMode()) {
    Mage::log('inboxify_setup ended');
}
