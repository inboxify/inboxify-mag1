<?php
/**
 * Professio_Inboxify extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag1/blob/master/LICENSE
 * 
 * @category       Professio
 * @package        Professio_Inboxify
 * @copyright      Copyright (c) 2016 - 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag1/blob/master/LICENSE
 */

/**
 * Override of checkout agreements allowing newsletter sign-up 
 * for onepage checkout.
 *
 * @category    Professio
 * @package     Professio_Inboxify
 */
class Professio_Inboxify_Block_CheckoutAgreements
extends Mage_Checkout_Block_Agreements
{
    /**
     * Override block template
     *
     * @return string
     */
    protected function _toHtml()
    {
        $uri = filter_input(INPUT_SERVER, 'REQUEST_URI');
        
        if (substr_count($uri, 'onepage')) {
            $this->setTemplate('inboxify/onepage-agreements.phtml');
        }
        
        return parent::_toHtml();
    }

    /**
     * Check if customer is signed up
     * @return bool
     */
    public function isCurrentCustomerSignedUp()
    {
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            
            if ($customer) {
                $client = Mage::getSingleton('inboxify/client')
                    ->getClient();
                $contact = $client->getContact($customer->getEmail());
                
                if ($contact && !$contact->unsubscribed) {
                    return true;
                }
            }
        }
        
        return false;
    }
    
    /**
     * Check if sign-up is hidden
     * @return bool
     */
    public function isSignupHidden()
    {
        return Professio_Inboxify_Model_Config_Source_Account::HIDDENCHECKED
            == Mage::helper('inboxify/config')->getAdvancedCreateAccount();
    }
    
    /**
     * Check if sign-up is checked
     * @return bool
     */
    public function isSignupChecked()
    {
        $v = Mage::helper('inboxify/config')->getAdvancedCreateAccount();
        
        return 
            Professio_Inboxify_Model_Config_Source_Account::HIDDENCHECKED
            == $v
            || Professio_Inboxify_Model_Config_Source_Account::CHECKED
            == $v;
    }
    
    /**
     * Get config helper 
     * @return Professio_Inboxify_Helper_Config
     */
    public function getConfigHelper()
    {
        return Mage::helper('inboxify/config');
    }
}
