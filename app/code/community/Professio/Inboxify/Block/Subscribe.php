<?php
/**
 * Professio_Inboxify extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag1/blob/master/LICENSE
 * 
 * @category       Professio
 * @package        Professio_Inboxify
 * @copyright      Copyright (c) 2016 - 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag1/blob/master/LICENSE
 */

/**
 * Newsletter subscription form block
 *
 * @category    Professio
 * @package     Professio_Inboxify
 */
class Professio_Inboxify_Block_Subscribe extends Mage_Core_Block_Template
{
    /**
     * Retrieve form action url and set "secure" param to avoid confirm
     * message when we submit form from secure page to unsecure
     *
     * @return string
     */
    public function getFormActionUrl()
    {
        return $this->getUrl(
            'inboxify/subscriber/subscribe', 
            array('_secure' => true)
        );
    }
    
    /**
     * Get config helper
     * 
     * @return Professio_Inboxify_Helper_Config
     */
    public function getConfigHelper()
    {
        return Mage::helper('inboxify/config');
    }
}
