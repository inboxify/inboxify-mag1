<?php
/**
 * Professio_Inboxify extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag1/blob/master/LICENSE
 * 
 * @category       Professio
 * @package        Professio_Inboxify
 * @copyright      Copyright (c) 2016 - 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag1/blob/master/LICENSE
 */

/**
 * Newsletter management block
 *
 * @category    Professio
 * @package     Professio_Inboxify
 */
class Professio_Inboxify_Block_Newsletter extends Mage_Core_Block_Template
{
    /**
     * Currenct contact
     * 
     * @var Professio_Inboxify_Model_Contact 
     */
    protected $_contact;
    
    /**
     * Constructor... set template
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->setTemplate('inboxify/newsletter.phtml');
    }

    /**
     * Get URL for the form
     * 
     * @return string
     */
    public function getAction()
    {
        return $this->getUrl('*/*/save');
    }
    
    /**
     * Get config helper
     * 
     * @return Professio_Inboxify_Helper_Config
     */
    public function getConfigHelper()
    {
        return Mage::helper('inboxify/config');
    }
    
    /**
     * Get current customer 
     * 
     * @return Mage_Customer_Model_Customer
     */
    protected function getCustomer()
    {
        return Mage::getSingleton('customer/session')->getCustomer();
    }
    
    /**
     * Check if current contact is subscribed
     * 
     * @return boolean
     */
    public function isSubscribed()
    {
        if (!isset($this->isSubscribed)) {
            $contact = Mage::getSingleton('inboxify/client')->getClient()
                ->getContact(
                    $this->getCustomer()->getEmail(),
                    $this->getConfigHelper()->getGeneralList()
                );
            
            $this->isSubscribed = $contact ? !$contact->unsubscribed : false;
        }
        
        return $this->isSubscribed;
    }
}
